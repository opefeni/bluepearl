<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 2/16/17
 * Time: 11:34 PM
 */

namespace App\Repository\Interfaces;



interface OtherRepositoryInterface
{
    public function create(array $data);

    public function edit($model, array $data);
}