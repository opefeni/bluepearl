<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 1/29/17
 * Time: 6:05 PM
 */

namespace App\Repository\Interfaces;



interface PinRepositoryInterface
{
    public function create(array $data);

    public function edit($model, array $data);
}