<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 1/29/17
 * Time: 6:02 PM
 */

namespace App\Repository;

use App\Model\pin;
use App\Repository\Interfaces\PinRepositoryInterface;

class PinRepository extends AbstractRepository implements PinRepositoryInterface
{
    protected $user;

    public function __construct(pin $model)
    {
        parent::__construct($model);
    }

    public function create(array $data)
    {
        $model = $this->getNew($data);

        $model->save();

        return $model;
    }


    public function edit($model, array $data)
    {
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function getCurrentActivities($id, $week)
    {
        return $this->model->where('staff_id',$id)
            ->where('week_no',$week);

    }

    public function getCreationForm(){
        return new ActivitiesForm();
    }

    public function findCount($id, $status)
    {
        return $this->model->where('staff_id',$id)
            ->where('status',$status)->count();
    }




    public function getCurrentDataTables(){
        return $this->model->select(array(
            'id',
            'activity_date',
            'job_no_dept',
            'project_id',
            'activity_type_id',
            'status',
            'duration'
        ))
            ->whereIn('status',[1,4])
            ->where('staff_id',Auth::user()->id)
            ->where('week_no',date('W'))
            ->orderBy('id', 'desc');
    }

  public function getTeamActivityDataTables(){
        return $this->model->select(array(
            'week_no',
            'year_no',
            DB::raw("count(teamID), count('week_no'), sum(duration) as duration")
        ))
            ->where('status',2)
            ->where('year_no',date('Y'))
            ->where('teamID',Auth::user()->teamID)
            ->groupBy('week_no')
            ->orderBy('id', 'desc');
    }






}