<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 2/16/17
 * Time: 11:33 PM
 */

namespace App\Repository;


use App\Repository\Interfaces\EducationRepositoryInterface;
use App\Model\education;
use App\Repository\AbstractRepository;

class EducationRepository extends AbstractRepository implements EducationRepositoryInterface
{

    protected $personal;

    public function __construct(education $model)
    {
        parent::__construct($model);
    }

    public function create(array $data)
    {
        $model = $this->getNew($data);

        $model->save();

        return $model;
    }


    public function edit($model, array $data)
    {
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function getSecondaryColumns(){

        return array(
            'SecName' => column_array('School Name','text','',null,null,true),
            'SecLoc' => column_array('Location','text','',null,null,true),
            'SecEntYr' => column_array('Entrance Year','text','',null,null,false),
            'SecExitYr'=> column_array('Exit Year','text','',null,null,true),
            'SecCert'=> column_array('Certificate Obtained','text','',null,null,true),

        );

    }
    public function getTerColumn(){

        return array(
            'TerName1' => column_array('Institution \'s name #1','text','',null,null,true),
            'TerLoc1' =>  column_array('Location #1','text','',null,null,true),
            'TerEntYr1'=>  column_array('Admission Year #1','text','',null,null,true),
            'TerExtYr1'=>  column_array('Graduation Year #1','text','',null,null,true),
            'TerCert1'=>  column_array('Qualification Obtained #1','text','',null,null,true),
            'TerMatric1'=>  column_array('Matriculation No. #1','text','',null,null,true),
            'TerName2' => column_array('Institution \'s name #2','text','',null,null,false),
            'TerLoc2' =>  column_array('Location #2','text','',null,null,false),
            'TerEntYr2'=>  column_array('Admission Year #2','text','',null,null,false),
            'TerExtYr2'=>  column_array('Graduation Year #2','text','',null,null,false),
            'TerCert2'=>  column_array('Qualification Obtained #2','text','',null,null,false),
            'TerMatric2'=>  column_array('Matriculation No. #2','text','',null,null,false),



        );

    }
    public function getPostTerColumns(){

        return array(
            'PosName1' => column_array('Institution \'s name #2','text','',null,null,false),
            'PosLoc1' =>  column_array('Location #2','text','',null,null,false),
            'PosEntYr1'=>  column_array('Admission Year #2','text','',null,null,false),
            'PosExtYr1'=>  column_array('Graduation Year #2','text','',null,null,false),
            'PosCert1'=>  column_array('Qualification Obtained #2','text','',null,null,false),
            'PosMatric1'=>  column_array('Matriculation No. #2','text','',null,null,false),
            'PosName2' => column_array('Institution \'s name #2','text','',null,null,false),
            'PosLoc2' =>  column_array('Location #2','text','',null,null,false),
            'PosEntYr2'=>  column_array('Admission Year #2','text','',null,null,false),
            'PosExtYr2'=>  column_array('Graduation Year #2','text','',null,null,false),
            'PosCert2'=>  column_array('Qualification Obtained #2','text','',null,null,false),
            'PosMatric2'=>  column_array('Matriculation No. #2','text','',null,null,false),


        );

    }
    public function getNyscColumns(){

        return array(
            'NyscSerDate' => column_array('Service Date','date','',null,null,false),
            'NyscCertNo' =>  column_array('NYSC Certificate No.','text','',null,null,false),
            'NyscStateCode'=>  column_array('State Code','text','',null,null,false),
            'NyscCallUpNo'=>  column_array('Call Up No.','text','',null,null,false),
            'NyscPPA'=>  column_array(' 	  	  	 
Place of Primary Assignment','text','',null,null,false),
            'NyscStartDate'=>  column_array('Start Date','date','',null,null,false),
            'NyscPOP' => column_array('Passing Out Date','date','',null,null,false),

        );

    }

    public function getProfessionalColumns(){

        return array(
            'ProfName' => column_array('Name of Association','text','',null,null,false),
            'ProfStatus' => column_array('Membership Status','text','',null,null,false),
            'ProfStartDate' => column_array('Membership Start-off Date ','date','',null,null,false),
            'ProfNo' => column_array('Membership No ','text','',null,null,false),

        );

    }

    public function getEmploymentColumns(){

        return array(
            'EmployeeName' => column_array(' Name of Employee','text','',null,null,false),
            'EmployeeAddress' => column_array('Address','text','',null,null,false),
            'EmployeeType' => column_array('Type of Business ','text','',null,null,false),
            'EmployeDate' => column_array('Date Employed  ','date','',null,null,false),
            'ExitDate' => column_array('Exit Date ','date','',null,null,false),
            'StartSalary' => column_array('Starting Salary','text','',null,null,false),
            'LastSalary' => column_array('Last Salary','text','',null,null,false),
            'StartPost' => column_array('Starting Designation','text','',null,null,false),
            'LastPost' => column_array('Last Designation','text','',null,null,false),
            'Duties' => column_array('Brief description of duties ','textarea','',null,null,false),
            'LeavingReasons' => column_array('Reasons for leaving','textarea','',null,null,false),
            'ContactName' => column_array('Contact Person\'s Name','text','',null,null,false),
            'ContactPhone' => column_array('Contact Person\'s Phone','text','',null,null,false),
            'ContactPosition' => column_array('Contact Person\'s Designation','text','',null,null,false),
            'EmpDebt' => column_array('Are you indebted to your previous employers:','select','',null,null,false),

        );

    }
    public function getOtherColumns()
    {
        return array(
            'DebtName' => column_array('Employer\'s Name','text','',null,null,false),
            'DebtAmt' => column_array('Debt Amount','text','',null,null,false),
            'DebtDate' => column_array('Date Incurred','date','',null,null,false),
        );
    }
}