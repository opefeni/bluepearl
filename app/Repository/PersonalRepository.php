<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 2/16/17
 * Time: 11:33 PM
 */

namespace App\Repository;


use App\Repository\Interfaces\PersonalRepositoryInterface;
use App\Model\personal;
use App\Repository\AbstractRepository;

class PersonalRepository extends AbstractRepository implements PersonalRepositoryInterface
{

    protected $personal;

    public function __construct(personal $model)
    {
        parent::__construct($model);
    }

    public function create(array $data)
    {
        $model = $this->getNew($data);

        $model->save();

        return $model;
    }


    public function edit($model, array $data)
    {
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function getPersonalColumns(){

        return array(
            'FirstName' => column_array('First Name','text','',null,null,true),
            'LastName' => column_array('Last Name','text','',null,null,true),
            'PreviousName' => column_array('Previous Name','text','',null,null,false),
            'Gender'=> column_array('Gender','select','',null,null,true),
            'DateOfBirth'=> column_array('Date Of Birth','date','',null,null,true),
            'PlaceOfBirth'=> column_array('Place Of Birth','text','',null,null,true),
            'Status'=> column_array('Marital Status','select','',null,null,true),
            'Email'=> column_array('Email','email','',null,null,true),
            'ResidentAddress'=> column_array('Residential Address','textarea','',null,null,true),
            'StateOfOrigin'=> column_array('State Of Origin','select','',null,null,true),
            'Nationality'=> column_array('Nationality','text','',null,null,true),
            'MobileNo'=> column_array('Mobile Number','text','',null,null,true),
            'SocialNo'=> column_array('Social Number','text','',null,null,true),

        );

    }
    public function getFamilyColumn(){

        return array(
            'FatherName' => column_array('Father \'s name','text','',null,null,true),
            'FatherStatus' =>  column_array('Father \'s status','select','',null,null,true),
            'FatherOffice'=>  column_array('Father \'s Office','text','',null,null,true),
            'FatherAddress'=>  column_array('Father \'s Address','text','',null,null,true),
            'FatherAge'=>  column_array('Father \'s Age','number','',null,null,true),
            'FatherPhone'=>  column_array('Father \'s Phone','text','',null,null,true),
            'MotherName' => column_array('Mother \'s name','text','',null,null,true),
            'MotherStatus' =>  column_array('Mother \'s status','select','',null,null,true),
            'MotherOffice'=>  column_array('Mother \'s Office','text','',null,null,true),
            'MotherAddress'=>  column_array('Mother \'s Address','text','',null,null,true),
            'MotherAge'=>  column_array('Mother \'s Age','number','',null,null,true),
            'MotherPhone'=>  column_array('Mother \'s Phone','text','',null,null,true),
            'SpouseName' => column_array('Spouse \'s name','text','',null,null,true),
            'SpouseStatus' =>  column_array('Spouse \'s status','select','',null,null,true),
            'SpouseOffice'=>  column_array('Spouse \'s Office','text','',null,null,true),
            'SpouseAddress'=>  column_array('Spouse \'s Address','text','',null,null,true),
            'SpouseAge'=>  column_array('Spouse \'s Age','number','',null,null,true),
            'SpousePhone'=>  column_array('Spouse \'s Phone','text','',null,null,true),


        );

    }
    public function getChildColumns(){

        return array(
            'ChildName1' => column_array('Child Name #1','text','',null,null,false),
            'ChildAge1' => column_array('Child Age #1','number','',null,null,false),
            'ChildGender1' => column_array('Child Gender #1','select','',null,null,false),
            'ChildName2' => column_array('Child Name #2','text','',null,null,false),
            'ChildAge2' => column_array('Child Age #2','number','',null,null,false),
            'ChildGender2' => column_array('Child Gender #2','select','',null,null,false),
            'ChildName3' => column_array('Child Name #3','text','',null,null,false),
            'ChildAge3' => column_array('Child Age #3','number','',null,null,false),
            'ChildGender3' => column_array('Child Gender #3','select','',null,null,false),
            'ChildName4' => column_array('Child Name #4','text','',null,null,false),
            'ChildAge4' => column_array('Child Age #4','number','',null,null,false),
            'ChildGender4' => column_array('Child Gender #4','select','',null,null,false),


        );

    }

    public function getOtherColumns(){

        return array(
            'MPNFamily' => column_array('Does any of your family work with MPN?','select','',null,null,true),
            'MPNRelationship' => column_array('if yes, relationship? ','select','',null,null,false),

        );

    }
}