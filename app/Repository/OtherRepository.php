<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 2/16/17
 * Time: 11:33 PM
 */

namespace App\Repository;


use App\Repository\Interfaces\OtherRepositoryInterface;
use App\Model\others;
use App\Repository\AbstractRepository;

class OtherRepository extends AbstractRepository implements OtherRepositoryInterface
{

    protected $personal;

    public function __construct(others $model)
    {
        parent::__construct($model);
    }

    public function create(array $data)
    {
        $model = $this->getNew($data);

        $model->save();

        return $model;
    }


    public function edit($model, array $data)
    {
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function getDebtColumns(){

        return array(
            'OrgDebt' => column_array('Are you indebted to any individual, association or Organization?','select','',null,null,false),
            'OrgDebtName' => column_array('Name','text','',null,null,false),
            'OrgDebtAmt' => column_array('Debt Amount','text','',null,null,false),
            'OrgDebtDate'=> column_array('Date Incurred ','date','',null,null,false),

        );

    }
    public function getBankColumns(){

        return array(
            'BankName1' => column_array('Bank name #1','text','',null,null,false),
            'BankBranch1' =>  column_array('Bank Branch #1','text','',null,null,false),
            'Lang1'=>  column_array('Language  #1','text','',null,null,false),
            'LangAbility1'=>  column_array('Abilities #1','select','',null,null,false),
            'BankName2' => column_array('Bank name #2','text','',null,null,false),
            'BankBranch2' =>  column_array('Bank Branch #2','text','',null,null,false),
            'Lang2'=>  column_array('Language  #2','text','',null,null,false),
            'LangAbility2'=>  column_array('Abilities #2','select','',null,null,false),

        );

    }
    public function getReferenceColumns(){
        return array(
            'RefName1' => column_array('Name #1','text','',null,null,true),
            'RefContact1' =>  column_array('Contact Address #1','text','',null,null,true),
            'RefEmail1'=>  column_array('Email #1','text','',null,null,true),
            'RefPhone1'=>  column_array('Phone #1','text','',null,null,true),
            'RefName2' => column_array('Name #2','text','',null,null,true),
            'RefContact2' =>  column_array('Contact Address #2','text','',null,null,true),
            'RefEmail2'=>  column_array('Email #2','text','',null,null,true),
            'RefPhone2'=>  column_array('Phone #2','text','',null,null,true),
            'RefName3' => column_array('Name #3','text','',null,null,true),
            'RefContact3' =>  column_array('Contact Address #3','text','',null,null,true),
            'RefEmail3'=>  column_array('Email #3','text','',null,null,true),
            'RefPhone3'=>  column_array('Phone #3','text','',null,null,true),
        );

    }
    public function getGeneralColumns(){

        return array(
            'RelAcptJob' => column_array('Do you have any relationship that could present a conflict of interest for you in accepting this job?','select','',null,null,false),
            'RelAcptReson' =>  column_array('Details','textarea','',null,null,false),
            'PosInSch'=>  column_array('Did you hold any position of responsibility while in Primary, Secondary, Tertiary, Post Tertiary or the Community?','select','',null,null,false),
            'PosInDet'=>  column_array('Details','textarea','',null,null,false),
            'Skill'=>  column_array('Do you possess any other skill(s) or qualification(s)? Yes/No','select','',null,null,false),
            'SkillDetails'=>  column_array('Details','textarea','',null,null,false),
            'Recreation' => column_array('Do you have recreational and community interest? Yes/No','select','',null,null,false),
            'RecreationDetail' => column_array('Details','textarea','',null,null,false),
            'Charges' => column_array('Have you ever been charged to a court of law?','select','',null,null,false),
            'CourtVerdict' => column_array('For what offense:','textarea','',null,null,false),
            'Jailed' => column_array('Have you ever been sentenced to serve jail terms? ','select','',null,null,false),
            'Prison' => column_array('Prison of Service','text','',null,null,false),
            'YrConvicted' => column_array('Year Convicted ','text','',null,null,false),
            'YrRelease' => column_array('Year Released ','text','',null,null,false),

        );

    }

    public function getDrivingColumns(){

        return array(
            'Drive' => column_array('Do you drive?','select','',null,null,false),
            'DriveArrested' => column_array('Have you ever been arrested for any driving offense?','select','',null,null,false),
            'Offence' => column_array('If yes, state the number of times and the offense(s ','textarea','',null,null,false),
            'DriveAccident' => column_array('Have you ever been involved in any accident(s) while driving?','select','',null,null,false),
            'DriveAccidentRpt' => column_array('If yes, give a brief report of how and where it occurred','textarea','',null,null,false),
            'License' => column_array('Do you have a valid driving license?','select','',null,null,false),
            'LicenseNo' => column_array('If yes, License No ','text','',null,null,false),
            'LicenseType' => column_array('License type','select','',null,null,false),
            'DateIssued' => column_array('Date issued ','date','',null,null,false),
            'ExpiredDate' => column_array('Expiry date','date','',null,null,false),
            'StateIssued' => column_array('State of issue','select','',null,null,false),
            'DateIssued1st' => column_array('Date of 1st issue:','date','',null,null,false),
            'OwnVehicle' => column_array('Do you have a vehicle/vehicle(s) of your own? ','select','',null,null,false),
            'RegistrationNo' => column_array('Registration No ','text','',null,null,false),
            'InsuranceType' => column_array('Insurance Type ','text','',null,null,false),
            'VehicleDateIssued' => column_array('Date Purchase ','date','',null,null,false),
            'PlaceIssued' => column_array('Place Purchase ','text','',null,null,false),
            'ImportPort' => column_array('Importation Port ','text','',null,null,false),
            'PurchaseState' => column_array('Vehicle(s) purchase state','select','',null,null,false),

        );

    }


}