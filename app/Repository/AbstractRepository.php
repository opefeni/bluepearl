<?php

/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 1/29/17
 * Time: 6:01 PM
 */
namespace App\Repository;
use Illuminate\Database\Eloquent\Model;


abstract class AbstractRepository
{
    /**
     * The model to execute queries on.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;


    /**
     * Create a new repository instance.
     *
     * @param \Illuminate\Database\Eloquent\Model $model The model to execute queries on
     */
    public function __construct(Model $model)
    {
        $this->model = $model;

    }
    /**
     * Get a new instance of the model.
     *
     * @param  array  $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getNew(array $attributes = array())
    {
        return $this->model->newInstance($attributes);
    }

    public function findAll($orderColumn = 'title', $orderDir = 'asc', $cols = array('*'))
    {
        $model = $this->model
            ->orderBy($orderColumn, $orderDir)
            ->get($cols);
        return $model;
    }

    public function findAllWithoutGet($orderColumn = 'title', $orderDir = 'asc', $cols = array('*'))
    {
        $model = $this->model->get();
        //$obj = new Collection($model);
        return $model;
    }

    public function findLimit($limit, $orderColumn = 'title', $orderDir = 'asc', $cols = array('*'))
    {
        $model = $this->model
            ->orderBy($orderColumn, $orderDir)
            ->limit($limit)
            ->get($cols);

        return $model;
    }

    /**
     * Use this function to return an ordered list in alphabetical order
     * @param $key
     * @param $value
     * @param null $default
     * @return array
     */
    public function orderedList($key,$value,$default=NULL)
    {
        $model = $this->model->orderBy($value)->lists($value, $key);
        if($default !=NULL) $model = array_add($model,'', $default );
        return $model;
    }

    public function listAll($key,$value,$default=NULL)
    {
        $model = $this->model->lists($value, $key);
        if($default !=NULL) $model = array_add($model,'', $default );
        $model = $model->toArray();
        asort($model);//sort everything.... @P
        return $model;
    }

    /**
     * @author Okeke Paul
     * For listing all columns of a model in json format
     * @param array $list
     * @param null $order Specify the value it should orderded by
     * @return mixed
     */
    public function listAllToJson(array $list, $order=NULL)
    {
        $model = $this->model->select($list)->orderBy($order)->get($list);
        return $model->toJson(JSON_HEX_APOS);
    }

    public function listModelRelationIds($model,$relation){
        $get = $model->$relation()->get()->lists('id');
        return $get;
        /*
        $new_array = array();
        foreach($get as $g){
            $new_array[] = $g->id;
        }
        return $new_array;*/
    }
    public function delete($id)
    {
        $model = $this->findById($id);

        try {
            //lets check if the record exist...before deleting to avoid null pointer errors
            if($model!=null) {
                $model->delete();
            }
        } catch ( \Illuminate\Database\QueryException $e) {
            if($e->getCode() == 2292){
                \Notification::error('This entry cannot be deleted as it has dependent entries');
                return -1;
            }
        }
        return false;
    }
    public function findById($id, $cols = array('*')){
        return $this->model->find($id, $cols);
    }
    public function findBy($where,$value, $cols = array('*')){
        return $this->model->where($where,$value)->get($cols);
    }

    // use findby to retirieve single row
    public function findByFirst($where,$value, $cols = array('*')){
        return $this->model->where($where,$value)->first($cols);
    }

    /**
     * Use this method to order records from recently added to least recent
     * @param $where
     * @param $value
     * @param array $cols
     * @return mixed
     */
    public function findInRecentOrder($where,$value, $cols = array('*')){
        return $this->model->where($where,$value)->orderBy('created_at', 'desc')->get($cols);
    }

    /**
     * Use this method to list record in order by a column
     * @param $where
     * @param $value
     * @param array $cols
     * @param string $by
     * @return mixed
     */
    public function findAndOrderByCol($where,$value, $cols = array('*'), $by="name"){
        return $this->model->where($where,$value)->orderBy($by, 'asc')->get($cols);
    }

    public function findBySingle($where,$value){
        return $this->model->where($where,$value)->first();
    }

    public function findByWhere($where, $cols = array('*')){
        return $this->model->where($where)->get($cols);
    }

    public function findBySearch($data, $cols = array('*')){
        $model = $this->model;
        return $model->get($cols);
    }

    public function findByIdWith($id, $cols = array('*'),$with=[]){
        return $this->model->with($with)->find($id, $cols);
    }

    public function findByWith($where,$value, $with, $cols = array('*')){
        return $this->model->where($where,$value)->with($with)->get($cols);
    }


}