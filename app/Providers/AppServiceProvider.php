<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Repository\Interfaces\PinRepositoryInterface','App\Repository\PinRepository');
        $this->app->bind('App\Repository\Interfaces\PersonalRepositoryInterface','App\Repository\PersonalRepository');
        $this->app->bind('App\Repository\Interfaces\EducationRepositoryInterface','App\Repository\EducationRepository');
        $this->app->bind('App\Repository\Interfaces\OtherRepositoryInterface','App\Repository\OtherRepository');

    }
}
