<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class personal extends Model
{
    protected $table = "personal";

    protected $guarded = ['save','save_exit'];

}
