<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class pin extends Model
{
    protected $table = "pin";

    protected $guarded = ['save'];
}
