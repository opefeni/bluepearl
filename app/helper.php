<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 2/15/17
 * Time: 11:12 PM
 */

function gender()
{
    return array(
        ''=>'--Please Select--',
        'Male'=> 'Male',
        'Female' => 'Female'
    );
}

function checkApplicationStatus($status)
{
    if($status != 0)
        return true;

}

function licence()
{
    return array(
        ''=>'--Please Select--',
        'B'=> 'B',
        'C' => 'C',
        'D' => 'D',
        'E' => 'E',
    );
}

function ability()
{
    return array(
        ''=>'--Please Select--',
        'Reading Only'=> 'Reading Only',
        'Speaking Only' => 'Speaking Only',
        'Writing Only' => 'Writing Only',
        'Reading and Writing Only' => 'Reading and Writing Only',
        'Reading and Writing and Speaking Only' => 'Reading and Writing and Speaking Only',
    );
}

function states()
{
  return  array( ''=>'--Please Select--',
        'Abuja'=>'Abuja',
        'Anambra'=>    'Anambra',
        'Enugu'=>    'Enugu',
        'Akwa Ibom'=>	'Akwa Ibom',
        'Adamawa'=>	'Adamawa',
        'Abia'=>	'Abia',
   'Bauchi'=>   'Bauchi',
    'Bayelsa'=>    'Bayelsa',
    'Benue'=>    'Benue',
    'Borno'=>    'Borno',
    'Cross River'=>    'Cross River',
    'Delta'=>    'Delta',
    'Ebonyi'=>    'Ebonyi',
    'Edo'=>    'Edo',
    'Ekiti'=>    'Ekiti',
    'Gombe'=>    'Gombe',
    'Imo'=>    'Imo',
    'Jigawa'=>    'Jigawa',
    'Kaduna'=>    'Kaduna',
    'Kano'=>    'Kano',
    'Katsina'=>    'Katsina',
    'Kebbi'=>    'Kebbi',
    'Kogi'=>    'Kogi',
    'Kwara'=>    'Kwara',
    'Lagos'=>    'Lagos',
    'Nasarawa'=>    'Nasarawa',
    'Niger'=>    'Niger',
    'Ogun'=>    'Ogun',
    'Ondo'=>    'Ondo',
    'Osun'=>    'Osun',
    'Oyo'=>    'Oyo',
    'Plateau'=>    'Plateau',
    'Rivers'=>    'Rivers',
    'Sokoto'=>    'Sokoto',
    'Taraba'=>    'Taraba',
    'Yobe'=>    'Yobe',
    'Zamfara'=>    'Zamfara'
    );
}

function maritalStatus()
{
    return array(''=>'--Please Select--','Single'=>'Single','Married'=>'Married',
           'Divorced'=>'Divorced','Separated'=>'Separated');
}

function YesNo()
{
    return array(''=>'--Please Select--','Yes'=>'Yes','No'=>'No');
}

function relationship()
{
    return array(''=>'--Please Select--','Friend'=>'Friend','Sibling'=>'Sibling','Spouse'=>'Spouse');
}

function generate_form_inputs($columns,$select_array = null,$divide = 4,$for_edit=false){
    if(is_array($select_array)){
        extract($select_array);
    }

    $str = '';
    foreach($columns as $col=>$value){
        //dd($columns);
        //dd($col);
        //d($value);
        //dd($columns);
        $str .= '<div class="col-md-'.$divide.'">'.PHP_EOL;
        $str .= '<div class="form-group">'.PHP_EOL;
        $str .= '<div class="col-md-12">'.PHP_EOL;
        if($value['type'] != 'hidden') {
            $str .= '<label class="control-label '.$col.'" style="margin-bottom:5px">'.$value['title'].
                (($value['required']) ? '<span style="color:red"> *</span>': '').'</label>'.PHP_EOL;
        }

        switch ($value['type']) {
            case "select":
                //dd($$col);
                $list = $$col;
                if(is_array($list)) asort($list); else $list = null; //@adedolapo this ensures all list are sorted alphabetically, sln to 'Ensure the contents of all Listbox are sorted in ascending order.'
                $str .= Form::select($col,$list,null,array('class'=>'form-control', isRequired($value['required'])));
                if($value['placeholder']) $str .= '<span class="help-block align-right">'.$value['placeholder'].'</span>';
                break;
            case "select2":
                $list = $$col;
                if(is_array($list)) asort($list); else $list = null; //@adedolapo this ensures all list are sorted alphabetically, sln to 'Ensure the contents of all Listbox are sorted in ascending order.'
                $str .= Form::select($col,$list,null,array('class'=>'select2 full-width-fix','placeholder'=>$value['placeholder']));
                break;
            case "multiple":
                if($for_edit){
                    $a = $col.'_selected';
                    $v = $$a;
                }else{
                    $v = null;
                }
                $str .= Form::select($col.'[]',$$col,$v,array('multiple','class'=>'select2 full-width-fix','placeholder'=>$value['placeholder']));
                break;

            case "email":
                $str .= Form::email($col,null,array('class'=>'form-control', isRequired($value['required']), 'placeholder'=>$value['placeholder'],  'autocomplete'=>'off'));
                break;
            case "text":
                $str .= Form::text($col,null,array('class'=>'form-control','placeholder'=>$value['placeholder'],
                    'autocomplete'=>'off', isRequired($value['required'])));
                break;
            case "url":
                $str .= Form::url($col,null,array('class'=>'form-control','placeholder'=>$value['placeholder']));
                break;
            case "password":
                $str .= Form::password($col,array('class'=>'form-control','placeholder'=>$value['placeholder']));
                break;
            case "date":
                $str .= '<div class="input-group date">';
                $str .= Form::text($col,null,array('class'=>'form-control','placeholder'=>'Date Format: dd/mm/yyy' ,isRequired($value['required'])));
                $str .= ' <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span></div>';
                break;
            case "date2"://use this date if you will be initializing the date somewhere else e.g for validation etc @paul
                $str .= '<div class="input-group date2">';
                $str .= Form::text($col,null,array('class'=>'form-control','placeholder'=>'Date Format: dd/mm/yyy','readonly',isRequired($value['required'])));
                $str .= ' <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span></div>';
                break;
            case "textarea":
                $str .= Form::textarea($col,null,array('class'=>'form-control','rows'=>'3','placeholder'=>$value['placeholder'],isRequired($value['required'])));
                break;
            case "file":
                $str .= '<input type="file" name="'.$col.'" '.isRequired($value['required']).' class="form-control"><div class="spacer-10"></div>';
                if($value['placeholder'] !=''){
                    $str .= '<span style="color:red;">'.$value['placeholder'].'</span>';
                }else{
                    $str .= '<span style="color:red;">Max file size: 20mb</span>';
                    $str .= '<div></div>';
                    $str .= '<span style="color:red;">Formats Allowed: png, jpg, jpeg,gif,pdf,doc</span>';
                }
                break;
            case "file_pdf":
                $str .= '<input type="file" name="'.$col.'" '.isRequired($value['required']).' class="form-control"><div class="spacer-10"></div>';
                if($value['placeholder'] !=''){
                    $str .= '<span style="color:red;">'.$value['placeholder'].'</span>';
                }else{
                    $str .= '<span style="color:red;">Max file size: 20mb</span>';
                    $str .= '<div></div>';
                    $str .= '<span style="color:red;">Formats Allowed: pdf</span>';
                }
                break;
            case "file_xls":
                $str .= '<input type="file" name="'.$col.'" '.isRequired($value['required']).' class="form-control"><div class="spacer-10"></div>';
                if($value['placeholder'] !=''){
                    $str .= '<span style="color:red;">'.$value['placeholder'].'</span>';
                }else{
                    $str .= '<span style="color:red;">Max file size: 20mb</span>';
                    $str .= '<div></div>';
                    $str .= '<span style="color:red;">Formats Allowed: Excel(xls)</span>';
                }
                break;
            case "database_file":
                $str .= '<input type="file" name="'.$col.'"><div class="spacer-10"></div>';
                if($value['placeholder'] !=''){
                    $str .= '<span style="color:red;">'.$value['placeholder'].'</span>';
                }else{
                    $str .= '<span style="color:red;">Max file size: 20mb; (Allowed formats: png, jpg, jpeg,gif,pdf,doc)</span>';
                }
                break;
            case "hidden":
                $str .= '<input type="hidden" name="'.$col.'" id="'.$col.'"><div class="spacer-10"></div>';
                break;
            case "hidden2":
                $str .= Form::hidden($col);
                break;
            case "checkbox":
                $str .= Form::checkbox($col).Form::hidden($col, 0);
                break;
            case "number":
                $str .= Form::input('number', $col, null, array('class'=>'form-control',isRequired($value['required'])));
                break;
            case "time":
                $str.= '<div class="input-group">';
                $str .= Form::input('number', 'hours', null, array('class'=>'form-control','placeholder'=>'HH','style'=>'width:50%',isRequired($value['required'])));
                $str .= Form::input('number', 'mins', null, array('class'=>'form-control','placeholder'=>'MM','style'=>'width:50%',isRequired($value['required'])));
                $str .= '</div>';
                break;
            default:
                $str .= Form::text($col,null,array('class'=>'form-control','placeholder'=>$value['placeholder'],
                    'autocomplete'=>'off', isRequired($value['required'])));
        }
        $str .= '</div>'.PHP_EOL;
        $str .= '</div>'.PHP_EOL;
        $str .= '</diV>'.PHP_EOL;
    }
    return $str;
}

function column_array($title,$type='text',$placeholder='',$relation=null,$relation_name='name',$required=false){
    return array(
        'title'=>$title,
        'type'=>$type,
        'placeholder'=>$placeholder,
        'relation'=>$relation,
        'relation_name'=>$relation_name,
        'required'=>$required
    );
}

function isRequired($required=false){
    if($required){
        return "required";
    }
    return "";
}