<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 2/17/17
 * Time: 10:37 AM
 */

namespace App\Http\Controllers;

use App\Repository\Interfaces\OtherRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class OtherController extends BaseController
{
    protected $other;
    public function __construct(OtherRepositoryInterface $other,Request $request)
    {
        $this->middleware('auth');
        $this->other = $other;
        $this->request = $request;
    }

    public function getIndex()
    {
        $this->data['items'] = $items = $this->other->findByFirst('user_id',Auth::user()->id);
        $this->data['debtColumns'] = $this->other->getDebtColumns();
        $this->data['bankColumns'] = $this->other->getBankColumns();
        $this->data['refColumns'] = $this->other->getReferenceColumns();
        $this->data['genColumns'] = $this->other->getGeneralColumns();
        $this->data['driveColumns'] = $this->other->getDrivingColumns();
        $this->data['select_array'] = array('LangAbility1'=>ability(),'LangAbility2'=>ability(),'OrgDebt'=>YesNo(),'RelAcptJob'=>YesNo(),
            'PosInSch'=>YesNo(),'Skill'=>YesNo(),'Recreation'=>YesNo(),'Charges'=>YesNo(),'Jailed'=>YesNo(),
            'Drive'=>YesNo(), 'DriveArrested'=>YesNo(), 'DriveAccident'=>YesNo(),'LicenseType'=>licence(),'License'=>YesNo(),
            'StateIssued' => states(), 'OwnVehicle'=>YesNo(), 'PurchaseState' => states());
        return view('other', $this->data);
    }

    public function postIndex()
    {
//        dd($this->request->all());
        $items = $this->other->findByFirst('user_id',Auth::user()->id);
        if(!is_null($items))
            $this->other->edit($items, $this->request->all());
        else
            $this->other->create($this->request->all());


        $message = 'Information Saved';
        return $this->redirectBack(array( 'message' =>$message ));
    }

    public function getSubmit()
    {
        $user= User::find(Auth::user()->id);
        $user->status = 1;
        $user->save();
        Auth::logout();
        Session::flush();
        $message = 'Your application is under review, you will not be able to edit this application again.';
        return $this->redirectRouteWithErrors('login',[],['email'=>$message]);
    }

}