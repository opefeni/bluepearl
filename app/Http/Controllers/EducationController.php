<?php
/**
 * Created by PhpStorm.
 * User: ajayi
 * Date: 2/17/17
 * Time: 10:37 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Interfaces\EducationRepositoryInterface;
use Illuminate\Support\Facades\Auth;


class EducationController extends BaseController
{
    protected $edu;
    public function __construct(EducationRepositoryInterface $edu,Request $request)
    {
        $this->middleware('auth');
        $this->edu = $edu;
        $this->request = $request;
    }

    public function getIndex()
    {
        $this->data['items'] = $items = $this->edu->findByFirst('user_id',Auth::user()->id);
        $this->data['secColumns'] = $this->edu->getSecondaryColumns();
        $this->data['terColumns'] = $this->edu->getTerColumn();
        $this->data['postColumns'] = $this->edu->getPostTerColumns();
        $this->data['nyscColumns'] = $this->edu->getNyscColumns();
        $this->data['profColumns'] = $this->edu->getProfessionalColumns();
        $this->data['empColumns'] = $this->edu->getEmploymentColumns();
        $this->data['otherColumns'] = $this->edu->getOtherColumns();
        $this->data['select_array'] = array('EmpDebt'=>YesNo());
        return view('biodata', $this->data);
    }

    public function postIndex()
    {
//        dd($this->request->all());
        $items = $this->edu->findByFirst('user_id',Auth::user()->id);
        if(!is_null($items))
            $this->edu->edit($items, $this->request->all());
        else
            $this->edu->create($this->request->all());


        $message = 'Education/Employment Information Saved';
        return $this->redirectBack(array( 'message' =>$message ));
    }

}