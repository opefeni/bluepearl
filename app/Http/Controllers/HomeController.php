<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Interfaces\PersonalRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $personal;
    public function __construct(PersonalRepositoryInterface $personal,Request $request)
    {
        $this->middleware('auth');
        $this->personal = $personal;
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->data['items'] = $items = $this->personal->findByFirst('user_id',Auth::user()->id);
        $this->data['personalColumns'] = $this->personal->getPersonalColumns();
        $this->data['familyColumns'] = $this->personal->getFamilyColumn();
        $this->data['childColumns'] = $this->personal->getChildColumns();
        $this->data['otherColumns'] = $this->personal->getOtherColumns();
        $this->data['select_array'] = array('Status'=>maritalStatus(),'Gender'=>gender(),'StateOfOrigin'=>states(),
            'ChildGender1'=>gender(),'ChildGender2'=>gender(),'ChildGender3'=>gender(),'ChildGender4'=>gender(),
            'MPNFamily'=>YesNo(), 'MPNRelationship'=>relationship(),'FatherStatus'=>maritalStatus(),'MotherStatus'=>maritalStatus(),'SpouseStatus'=>maritalStatus());
        return view('home', $this->data);
    }

    public function postIndex()
    {
//        dd($this->request->all());
        $items = $this->personal->findByFirst('user_id',Auth::user()->id);
        if(!is_null($items))
            $this->personal->edit($items, $this->request->all());
        else
            $this->personal->create($this->request->all());


        $message = 'Personal Profile Saved';
        return $this->redirectBack(array( 'message' =>$message ));
    }

}
