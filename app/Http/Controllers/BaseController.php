<?php
/**
 * Created by PhpStorm.
 * User: opefeni
 * Date: 11/22/16
 * Time: 2:45 AM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;

class BaseController extends Controller
{

    public $data = [];


    /**
     * Redirect to the specified named route.
     *
     * @param  string  $route
     * @param  array  $params
     * @param  array  $data
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectRoute($route, $params = array(), $data = array())
    {
        return Redirect::route($route, $params)->with($data);
    }

    /**
     * Redirect back with old input and the specified data.
     *
     * @param  array $data
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectBack($data = array())
    {
        return Redirect::back()->withInput()->with($data);
    }

    protected function redirectRouteWithErrors($route, $params = array(), $data = array())
    {
        return Redirect::route($route, $params)->withErrors($data);
    }

    /**
     * Redirect a logged in user to the previously intended url.
     *
     * @param  mixed $default
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectIntended($default = null)
    {
        return Redirect::intended($default);
    }
    protected function redirectTo($url)
    {
        return Redirect::to($url);
    }
}