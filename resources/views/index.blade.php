<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name','CV') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/glyphicons.css')}}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/datepicker.css')}}">
</head>
<body>
<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12  col-sm-12">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Personal Information</div>
                <div style="float:right; position: relative; top:-10px"><a href="#" class="button button-primary"><i class="glyphicon glyphicon-plus"> Add</i></a></div>
            </div>

            <div style="padding-top:30px" class="panel-body" >

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                <table width="929" border="0" cellpadding="0" cellspacing="0" align="center">

                <tr>
                    <td width="929" height="511" valign="top"><form id="form1" name="form1" method="post" action="">
                            <h1> Personal Information </h1><?php print "<br/>".@$response; ?>
                            <table width="900" border="0" align="center">
                                <tr>
                                    <td width="287">FirstName</td>
                                    <td width="292" colspan="3">LastName</td>
                                    <td width="307">MiddleName</td>
                                </tr>
                                <tr>
                                    <td><input name="FirstName" type="text" id="FirstName" value="<?php print $res['FirstName']; ?>" /></td>
                                    <td colspan="3"><input name="LastName" type="text" id="LastName"  value="<?php print $res['LastName']; ?>" /></td>
                                    <td><input name="PreviousName" type="text" id="PreviousName"  value="<?php print $res['PreviousName']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td colspan="3">Date of Birth </td>
                                    <td>PlaceOfBirth</td>
                                </tr>
                                <tr>
                                    <td><select name="gender" size="1" id="gender">
                                            <option value="<?php print $res['Gender']; ?>" selected="selected"><?php print $res['Gender']; ?></option>
                                            <option value="Female">Female</option>
                                            <option value="Male">Male</option>
                                        </select>          </td>
                                    <td><select name="day" size="1" id="day">
                                            <option value="<?php print $date[0]; ?>" selected="selected"><?php print $date[0]; ?></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>          </td>
                                    <td><select name="month" size="1" id="month">
                                            <option value="<?php print $date[1]; ?>" selected="selected"><?php print $date[1]; ?></option>
                                            <option value="Jan">Jan</option>
                                            <option value="Feb">Feb</option>
                                            <option value="Mar">Mar</option>
                                            <option value="Apr">Apr</option>
                                            <option value="May">May</option>
                                            <option value="Jun">Jun</option>
                                            <option value="Jul">Jul</option>
                                            <option value="Aug">Aug</option>
                                            <option value="Sep">Sep</option>
                                            <option value="Oct">Oct</option>
                                            <option value="Nov">Nov</option>
                                            <option value="Dec">Dec</option>
                                        </select></td>
                                    <td><select name="year" size="1" id="year">
                                            <option value="<?php print $date[2]; ?>" selected="selected"><?php print $date[2]; ?></option>

                                            <option value="2012">2012</option>
                                            <option value="2010">2010</option>
                                            <option value="2009">2009</option>

                                            <option value="2008">2008</option>
                                            <option value="2007">2007</option>
                                            <option value="2006">2006</option>
                                            <option value="2005">2005</option>
                                            <option value="2004">2004</option>
                                            <option value="2003">2003</option>
                                            <option value="2002">2002</option>

                                            <option value="2001">2001</option>

                                            <option value="2000">2000</option>

                                            <option value="1999">1999</option>

                                            <option value="1998">1998</option>

                                            <option value="1997">1997</option>

                                            <option value="1996">1996</option>

                                            <option value="1995">1995</option>

                                            <option value="1994">1994</option>

                                            <option value="1993">1993</option>

                                            <option value="1992">1992</option>

                                            <option value="1991">1991</option>

                                            <option value="1990">1990</option>

                                            <option value="1989">1989</option>

                                            <option value="1988">1988</option>

                                            <option value="1987">1987</option>

                                            <option value="1986">1986</option>

                                            <option value="1985">1985</option>

                                            <option value="1984">1984</option>

                                            <option value="1983">1983</option>

                                            <option value="1982">1982</option>

                                            <option value="1981">1981</option>

                                            <option value="1980">1980</option>

                                            <option value="1982">1982</option>

                                            <option value="1981">1981</option>

                                            <option value="1980">1980</option>

                                            <option value="1979">1979</option>

                                            <option value="1978">1978</option>

                                            <option value="1977">1977</option>

                                            <option value="1976">1976</option>

                                            <option value="1975">1975</option>

                                            <option value="1974">1974</option>

                                            <option value="1973">1973</option>

                                            <option value="1972">1972</option>

                                            <option value="1971">1971</option>

                                            <option value="1970">1970</option>

                                            <option value="1969">1969</option>

                                            <option value="1968">1968</option>

                                            <option value="1967">1967</option>

                                            <option value="1966">1966</option>

                                            <option value="1965">1965</option>

                                            <option value="1964">1964</option>

                                            <option value="1963">1963</option>

                                            <option value="1962">1962</option>

                                            <option value="1961">1961</option>

                                            <option value="1960">1960</option>

                                            <option value="1959">1959</option>

                                            <option value="1958">1958</option>

                                            <option value="1957">1957</option>

                                            <option value="1956">1956</option>

                                            <option value="1955">1955</option>

                                            <option value="1954">1954</option>

                                            <option value="1953">1953</option>

                                            <option value="1952">1952</option>

                                            <option value="1951">1951</option>

                                            <option value="1950">1950</option>

                                        </select></td>
                                    <td><input name="PlaceOfBirth" type="text" id="PlaceOfBirth" value="<?php print $res['PlaceOfBirth']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Marital Status </td>
                                    <td colspan="3">Email</td>
                                    <td>Residential Address </td>
                                </tr>
                                <tr>
                                    <td><select name="status" size="1" id="status">
                                            <option value="<?php print $res['Status']; ?>" selected="selected"><?php print $res['Status']; ?></option>
                                            <option value="Single">Single</option>
                                            <option value="Married">Married</option>
                                            <option value="Divorce">Divorce</option>
                                            <option value="Separated">Separated</option>
                                        </select></td>
                                    <td colspan="3"><input name="email" type="text" id="email" value="<?php print $res['Email']; ?>" /></td>
                                    <td><input name="address" type="text" id="address" value="<?php print $res['ResidentAddress']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>State of Origin </td>
                                    <td colspan="3">Nationality</td>
                                    <td>Mobile No </td>
                                </tr>
                                <tr>
                                    <td><select name="Origin" size="1" id="Origin" >

                                            <option>Abuja</option>
                                            <option>Anambra</option>
                                            <option>Enugu</option>
                                            <option>Akwa Ibom</option>
                                            <option>Adamawa</option>
                                            <option>Abia</option>
                                            <option> Bauchi</option>
                                            <option>Bayelsa</option>
                                            <option>Benue</option>
                                            <option>Borno</option>
                                            <option>Cross River</option>
                                            <option>Delta</option>
                                            <option>Ebonyi</option>



                                            <option>Edo</option>
                                            <option>Ekiti</option>
                                            <option>Gombe</option>
                                            <option>Imo</option>
                                            <option>Jigawa</option>
                                            <option>Kaduna</option>
                                            <option>Kano</option>
                                            <option>Katsina</option>
                                            <option>Kebbi</option>
                                            <option>Kogi</option>
                                            <option>Kwara</option>
                                            <option>Lagos</option>
                                            <option>Nasarawa</option>



                                            <option>Niger</option>
                                            <option>Ogun</option>
                                            <option>Ondo</option>
                                            <option>Osun</option>
                                            <option>Oyo</option>
                                            <option>Plateau</option>
                                            <option>Rivers</option>
                                            <option>Sokoto</option>
                                            <option>Taraba</option>
                                            <option>Yobe</option>
                                            <option>Zamfara</option>

                                        </select></td>
                                    <td colspan="3"><input name="nationality" type="text" id="nationality" value="<?php print $res['Nationality']; ?>" /></td>
                                    <td><input name="Phone" type="text" id="Phone" value="<?php print $res['MobileNo']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>National ID/Social No </td>
                                    <td colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="SocialNo" type="text" id="SocialNo" value="<?php print $res['SocialNo']; ?>" /></td>
                                    <td colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>




            </div>
        </div>
    </div>
    <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Sign Up</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
            </div>
            <div class="panel-body" >
                <form id="signupform" class="form-horizontal" role="form">

                    <div id="signupalert" style="display:none" class="alert alert-danger">
                        <p>Error:</p>
                        <span></span>
                    </div>



                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" placeholder="Email Address">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="firstname" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="firstname" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="passwd" placeholder="Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="icode" class="col-md-3 control-label">Invitation Code</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="icode" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                            <span style="margin-left:8px;">or</span>
                        </div>
                    </div>

                    <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">

                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>   Sign Up with Facebook</button>
                        </div>

                    </div>



                </form>
            </div>
        </div>




    </div>
</div>

<!-- JavaScripts -->
<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>

</body>
</html>
