<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name','CV') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/glyphicons.css')}}">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <link rel="stylesheet" href="{{asset('css/datepicker.css')}}">
    </head>
    <body>
    <div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title">Sign In</div>
                </div>

                <div style="padding-top:30px" class="panel-body" >

                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                    <form id="loginform" class="form-horizontal" method="post" role="form">

                        {{--<div style="margin-bottom: 25px" class="input-group">--}}
                            {{--<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>--}}
                            {{--<input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username or email">--}}
                        {{--</div>--}}

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input id="login-password" type="text" class="form-control" name="pin" placeholder="Pin Number">
                        </div>



                        {{--<div class="input-group">--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input id="login-remember" type="checkbox" name="remember" value="1"> Remember me--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        <div style="margin-top:10px" class="form-group">
                            <!-- Button -->

                            <div class="col-sm-12 controls pull-right">
                                <input type="submit" id="btn-login" class="btn btn-success" value="Login" style="width: 100%">

                            </div>
                        </div>



                    </form>



                </div>
            </div>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>

    </body>
</html>
