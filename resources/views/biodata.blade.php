@extends('layouts.app')

@section('content')
<div class="container">
    @include('partials.notify')
    @include('partials.notify_success')
    {{ Form::model($items,array('class'=>'form-horizontal row-border', 'id'=>'editResearchForm'))}}
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Educational History</h3></div>

                <div class="panel-body">
                    <h4>SECONDARY SCHOOL RECORDS </h4><hr>
                    {!! generate_form_inputs($secColumns,$select_array,4) !!}
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                </div>

                <div class="panel-body">
                    <h4>TERTIARY INSTITUTION RECORDS </h4><hr>
                    {!! generate_form_inputs($terColumns,$select_array,3) !!}
                </div>

                <div class="panel-body">
                    <h4>POST TERTIARY INSTITUTION </h4><hr>
                    {!! generate_form_inputs($postColumns,$select_array,3) !!}
                </div>

                <div class="panel-body">
                    <h4>NATIONAL YOUTH SERVICE HISTORY </h4><hr>
                    {!! generate_form_inputs($nyscColumns,$select_array,4) !!}
                </div>
                <div class="panel-body">
                    <h4>PROFESSIONAL ASSOCIATION INSTITUITION OR SOCIETY MEMBERSHIP </h4><hr>
                    {!! generate_form_inputs($profColumns,$select_array,3) !!}
                </div>
            </div>
        </div>


    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>PREVIOUS EMPLOYMENT HISTORY</h3></div>
                <div class="panel-body">
                    {!! generate_form_inputs($empColumns,$select_array,4) !!}
                </div>
                <div class="panel-body">
                    <p> If yes, please state the name of the employer and debt particulars:</p>
                    {!! generate_form_inputs($otherColumns,$select_array,4) !!}
                </div>
            </div>
        </div>
        <div class="form-actions text-center" style="margin-bottom: 10px" >
            <a href="{{ url('home') }}" class="btn btn-default btn-lg">Previous </a>&#160;&#160;&#160;
            <input type="submit" value="Save" class="btn btn-success btn-lg" name="save">&#160;&#160;&#160;

            <a href="{{ url('other') }}" class="btn btn-default btn-lg">Next </a>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
