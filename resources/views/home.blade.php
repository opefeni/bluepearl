@extends('layouts.app')
@section('content')
<div class="container">
    @include('partials.notify')
    @include('partials.notify_success')
    {{ Form::model($items,array('class'=>'form-horizontal row-border', 'id'=>'editResearchForm'))}}
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Personal Information</h3></div>

                <div class="panel-body">
                    {!! generate_form_inputs($personalColumns,$select_array,4) !!}
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Family Information</h3></div>

                <div class="panel-body">
                    {!! generate_form_inputs($familyColumns,$select_array,4) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Children Information</h3></div>

                <div class="panel-body">
                    {!! generate_form_inputs($childColumns,$select_array,4) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Other Information</h3></div>

                <div class="panel-body">
                    {!! generate_form_inputs($otherColumns,$select_array,4) !!}
                </div>
            </div>
        </div>
        <div class="form-actions text-center" style="margin-bottom: 10px" >
            <input type="submit" value="Save" class="btn btn-success btn-lg" name="save">&#160;&#160;&#160;
                {{--<input type="submit" value="Save & Submit NCDMB" class="btn btn-danger btn-lg" name="save">&#160;&#160;&#160;--}}

            <a href="{{ url('biodata') }}" class="btn btn-default btn-lg">Next </a>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
