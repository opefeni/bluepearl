@extends('layouts.app')

@section('content')
<div class="container">
    @include('partials.notify')
    @include('partials.notify_success')
    {{ Form::model($items,array('class'=>'form-horizontal row-border', 'id'=>'editResearchForm'))}}
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Other Information</h3></div>

                <div class="panel-body">
                    <h4> INDEBTEDNESS </h4><hr>
                    {!! generate_form_inputs($debtColumns,$select_array,3) !!}
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                </div>

                <div class="panel-body">
                    <h4>NAME OF BANKERS & LANGUAGE ABILITIES  </h4><hr>
                    {!! generate_form_inputs($bankColumns,$select_array,3) !!}
                </div>

                <div class="panel-body">
                    <h4>REFEREES-please provide the details of 3 referees we may contact: </h4><hr>
                    {!! generate_form_inputs($refColumns,$select_array,3) !!}
                </div>

                <div class="panel-body">
                    <h4>GENERAL </h4><hr>
                    {!! generate_form_inputs($genColumns,$select_array,6) !!}
                </div>
                <div class="panel-body">
                    <h4>DRIVING HISTORY</h4><hr>
                    {!! generate_form_inputs($driveColumns,$select_array,4) !!}
                </div>
            </div>
        </div>


    </div>

    <div class="row">
        {{--<div class="col-md-12 ">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading"><h3>PREVIOUS EMPLOYMENT HISTORY</h3></div>--}}
                {{--<div class="panel-body">--}}
                    {{--{!! generate_form_inputs($empColumns,$select_array,4) !!}--}}
                {{--</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<p> If yes, please state the name of the employer and debt particulars:</p>--}}
                    {{--{!! generate_form_inputs($otherColumns,$select_array,4) !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="form-actions text-center" style="margin-bottom: 10px" >
            <a href="{{ url('biodata') }}" class="btn btn-default btn-lg">Previous </a>&#160;&#160;&#160;
            <input type="submit" value="Save" class="btn btn-success btn-lg" name="save">&#160;&#160;&#160;

            <button data-toggle="confirmation" data-title=" Are you sure ?" data-content="You will no longer be able to edit this form again once finished. "
               data-btn-ok-label="Continue" data-btn-ok-icon="glyphicon glyphicon-share-alt"
               data-btn-ok-class="btn-success"
               data-btn-cancel-label="Cancel!" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
               data-btn-cancel-class="btn-danger"
               href="{{ url('submit') }}" class="btn btn-default btn-lg finish">Finish </button>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
