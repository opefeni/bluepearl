

@if($errors->all())
       		   <div class="alert alert-danger fade in"><button type="button" class="pi-close" data-dismiss="alert"><i class="icon-cancel"></i></button>
       		        <h4>Please fix the errors below:</h4>
				   <ul>
					   @foreach($errors->all() as $error)
						   <li>{{ $error }}</li>
					   @endforeach
				   </ul>
       		    </div>
       		@endif
     