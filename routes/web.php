<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => 'auth'],function (){

    Route::get('/home', 'HomeController@index');
    Route::post('/home', 'HomeController@postIndex');

    Route::get('/biodata', 'EducationController@getIndex');
    Route::post('/biodata', 'EducationController@postIndex');

    Route::get('/other', 'OtherController@getIndex');
    Route::post('/other', 'OtherController@postIndex');
    Route::get('/submit', 'OtherController@getSubmit');
});



